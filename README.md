# Indigo 3.6 Remake
> Smef's Indigo 3.6 remake 

## Based off Indigo Remastered
This is not just a offset update
Because Indigo 4.0 is not *that* different than old indigo 3, i just remade it with the remastered source.Also this repo will have improvements

## Improvements
Now the font is inside the source, so you dont have to install the font manually.

## Issues
Thirdperson(broken)
Sniper crosshair(broken)
Skybox changer (broken or i couldnt add it correctly)
Custom Chams Materials (Glass,crystal,etc)
## Credits

[@smefpw](https://github.com/smefpw/) for the Indigo 3.6 and Indigo remastered source that made this project[@smefpw/mefs-Indigo-Remastered](https://github.com/smefpw/smefs-Indigo-Remastered)

## VAC Warning
Warning: This cheat uses VMT Hooking (for now)
I dont recommend running this on VAC Enabled servers, which may cause in a ban.


## Changelog

0.1 
Initial commit

0.2
Rainbow menu option added
Config system fixed
Disabled some options that doesnt work properly/doesnt work at all
"Added" Thirdperson (it is probably broken, didnt try it though)


0.3
disabled thirdperson until further notice
Added sniper crosshair

0.4
Removed thirdperson from the menu
added remove post processing
added cheat build